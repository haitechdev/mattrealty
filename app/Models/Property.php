<?php 

namespace Models;

 

use Core\Model;

 

class Property extends Model 

{    

    function __construct(){

        parent::__construct();

    }  

    

    public function getDistinctSuburbs()

    {

        return $this->db->select("SELECT DISTINCT suburb FROM ".PREFIX."properties ORDER BY suburb");

    } 

    

    public function getDistinctBedrooms()

    {

        return $this->db->select("SELECT DISTINCT bedrooms FROM ".PREFIX."properties ORDER BY bedrooms");

    }

    

    public function getInitialProperties()

    {

        return $this->db->select("SELECT * FROM ".PREFIX."properties WHERE active = 1 ORDER BY rand() LIMIT 10");     

    }

	

	public function getFeatureProperties()

    {

        return $this->db->select("SELECT * FROM ".PREFIX."properties WHERE featured = 1 ORDER BY rand() LIMIT 2");     

    }

	public function getFilteredProperties($where)
	{
		return $this->db->select("SELECT * FROM ".PREFIX."properties ". $where. " ORDER BY RAND() LIMIT 10");
	}

	public function getAllProperties()

    {

        return $this->db->select("SELECT * FROM ".PREFIX."properties ORDER BY id DESC LIMIT 20");     

    }


	public function getProperty($id)

    {

        return $this->db->select("SELECT * FROM ".PREFIX."properties WHERE id = :id", array(":id"=>$id));     

    }

}

