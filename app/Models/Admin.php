<?php 

namespace Models;

 

use Core\Model;
use Helpers\Database;

 

class Admin extends Model {    

    	function __construct(){
		parent::__construct();
	}  

    	public function userPassword($username){
		return $this->db->select("SELECT password FROM ".PREFIX."users WHERE username = $username");
	} 

	public function getHash($username){
		$data = $this->db->select("SELECT password FROM ".PREFIX."users WHERE username = :username", 
			array('username' => $username));

		return $data[0]->password;
	}

	public function getAdminProperties($limit){
		return $this->db->select('
		SELECT 
		*,
		(SELECT count(id) FROM '.PREFIX.'properties) as total
	 FROM '.PREFIX.'properties '.$limit);     
	}

	public function getListing($id){
		return $this->db->select("SELECT * FROM ".PREFIX."properties WHERE id =:id",array(':id' => $id));
	}
	

	
	
	public function getAdminDetails($id){
		return $this->db->select('SELECT * FROM '.PREFIX.'properties WHERE id = '. $id);
	}

	public function getListingView($id)
	{
		return $this->db->select("SELECT * FROM ".PREFIX."properties WHERE id = ". $where);
	}

	public function updateProperty($data, $where){
		$this->db->update(PREFIX."properties", $data, $where);
	}

	public function addProperty($data){
		$this->db->insert(PREFIX.'properties',$data);
	}   

	public function deleteProperty($id){
		$this->db->delete(PREFIX.'properties', array('id' => $id));
	} 
}