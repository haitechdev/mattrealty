<?php

	namespace Controllers;

	use Core\View;
	use Core\Controller;
	use Helpers\Session;
	use Helpers\Url;
	use Helpers\Paginator;

class Admin extends Controller{

	private $_listings;

	public function __construct(){
	        parent::__construct();
		$this->_listings = new \Models\Admin();
		if(!Session::get('loggedin')){
			Url::redirect('login');
		}		
	}

	public function admin(){
     
		if(isset($_POST['add_listing'])){
			$address = $_POST['street_address'];
			$suburb = $_POST['suburb'];
			$town_city = $_POST['town_city'];
			$bedrooms = $_POST['bedrooms'];
			$ensuite = $_POST['ensuite'];
			$pool = $_POST['pool'];
			if($_POST['is_active']){
				$active = 1;
			}else{
				$active = 0;
			}

			/*if(empty($address)){
				$error[] = 'Please enter address';
			}

			if(empty($suburb)){
				$error[] = 'Please enter suburb';
			}

			if(empty($town_city)){
				$error[] = 'Please enter town/city';
			}

			if(empty($bedrooms)){
				$error[] = 'Please enter bedrooms';
			}

			if(empty($ensuite)){
				$error[] = 'Please enter ensuite';
			}

			if(empty($pool)){
				$error[] = 'Please enter pool';
			}*/

			if(!isset($error)){
				$postdata = array(
					'address' => $address,
					'suburb' => $suburb,
					'town_city' => $town_city,
					'bedrooms' => $bedrooms,
					'ensuite' => $ensuite,
					'pool' => $pool,
					'active' => $active
				);
				$this->_listings->addListing($postdata);
				Url::redirect('admin');
			}
		}


		if(isset($_POST['edit_listing'])){
			$id = $_POST['id'];
			$address = $_POST['street_address'];
			$suburb = $_POST['suburb'];
			$town_city = $_POST['town_city'];
			$bedrooms = $_POST['bedrooms'];
			$ensuite = $_POST['ensuite'];
			$pool = $_POST['pool'];
			if($_POST['is_active']){
				$active = 1;
			}else{
				$active = 0;
			}

			/*if(empty($address)){
				$error[] = 'Please enter address';
			}

			if(empty($suburb)){
				$error[] = 'Please enter suburb';
			}

			if(empty($town_city)){
				$error[] = 'Please enter town/city';
			}

			if(empty($bedrooms)){
				$error[] = 'Please enter bedrooms';
			}

			if(empty($ensuite)){
				$error[] = 'Please enter ensuite';
			}

			if(empty($pool)){
				$error[] = 'Please enter pool';
			}*/

			if(!isset($error)){
				$postdata = array(
					'address' => $address,
					'suburb' => $suburb,
					'town_city' => $town_city,
					'bedrooms' => $bedrooms,
					'ensuite' => $ensuite,
					'pool' => $pool,
					'active' => $active
				);
				$where = $id;
				$this->_listings->updateListing($postdata, $where);
				Url::redirect('admin');
			}
		}



		$data['listing'] = $this->_listings->getListing($id);



        	$data['title'] = 'Admin';
        	$data['name'] = 'Matt';
		$pages = new \Helpers\Paginator('20','p');
		$data['listings'] = $this->_listings->getAdminProperties($pages->getLimit());
		$pages->setTotal($data['listings'][0]->total);  
		$data['pageLinks'] = $pages->pageLinks();



		
		
		
	
        	View::renderTemplate('header', $data);
        	View::render('admin/index', $data, $error);
        	View::renderTemplate('footer', $data);
    	}	

	public function login(){

		$data['title'] = 'Login';
		$data['name'] = 'Matt';

		View::renderTemplate('header', $data);
	        View::render('login', $data);
        	View::renderTemplate('footer', $data);
	}


	public function getFilteredProperties($id)
	{
		$data = file_get_contents('php://input');
		$data = json_decode($data);

		$where = "";
		
		if( $data->address !==""){
			$address = $data->address;
			$where.= "WHERE address LIKE '%$address%' ";
		}

		if( $data->suburb !=="" ){
			$suburb = $data->suburb;
			if($where != ""){
				$where.= "AND suburb = '$suburb' ";
			}else{
				$where.= "WHERE suburb = '$suburb' ";
			}
		}

		if( $data->bedrooms !=="" ){
			$bedrooms = $data->bedrooms;
			if($where != ""){
				$where.= "AND bedrooms = '$bedrooms' ";
			}else{
				$where.= "WHERE bedrooms = '$bedrooms' ";
			}
		}

		if( $data->ensuite ){
			$ensuite = $data->ensuite;
			if($where != ""){
				$where.= "AND ensuite > 0 ";
			}else{
				$where.= "WHERE ensuite > 0 ";
			}
		}

		if( $data->pool ){
			$pool = $data->pool;
			if($where != ""){
				$where.= "AND pool > 0 ";
			}else{
				$where.= "WHERE pool > 0 ";
			}
		}
		echo json_encode($this->properties->getFilteredProperties($id));

	}


	public function update()
	{
		$data = file_get_contents('php://input');
		$data = json_decode($data);

		if ($data->ensuite){
			$data->ensuite=1;
		}else{
			$data->ensuite=null;
		}
		if ($data->pool){
			$data->pool=1;
		}else{
			$data->pool=null;
		}
		if ($data->active){
			$data->active=1;
		}else{
			$data->active=null;
		}

		$postdata = array(
			'address'=> $data->address,
			'suburb'=> $data->suburb,
			'town_city'=> $data->town_city,
			'list_price'=> $data->list_price,
			'bedrooms'=> $data->bedrooms,
			'ensuite'=> $data->ensuite,
			'pool'=> $data->pool,
			'active'=> $data->active
		);

		$where = array('id' => $data->id);
		$this->_listings->updateProperty($postdata, $where);
	}

	public function add()
	{
		$data = file_get_contents('php://input');
		$data = json_decode($data);
		if ($data->ensuite){
			$data->ensuite=1;
		}else{
			$data->ensuite=null;
		}
		if ($data->pool){
			$data->pool=1;
		}else{
			$data->pool=null;
		}
		if ($data->active){
			$data->active=1;
		}else{
			$data->active=null;
		}

		$postdata = array(
			'address'=> $data->address,
			'suburb'=> $data->suburb,
			'town_city'=> $data->town_city,
			'list_price'=> $data->list_price,
			'bedrooms'=> $data->bedrooms,
			'ensuite'=> $data->ensuite,
			'pool'=> $data->pool,
			'active'=> $data->active
		);

		$this->_listings->addProperty($postdata);
	}

	public function delete()
	{
		$data = file_get_contents('php://input');
		$data = json_decode($data);

		$this->_listings->deleteProperty($data);
	}

}



