<?php

namespace Controllers;



use Core\View;

use Core\Controller;



class Home extends Controller

{

  

    public function __construct()

    {

        parent::__construct();

        $this->properties = new \Models\Property();

    }



    public function index()

    {     

        $data['title'] = 'Matt';



              

        View::renderTemplate('header', $data);

        View::render('home', $data);

        View::renderTemplate('footer', $data);

    } 

	public function getFilteredProperties()
	{
		$data = file_get_contents('php://input');
		$data = json_decode($data);

		$where = "";
		
		if( $data->address !==""){
			$address = $data->address;
			$where.= "WHERE address LIKE '%$address%' ";
		}

		if( $data->suburb !=="" ){
			$suburb = $data->suburb;
			if($where != ""){
				$where.= "AND suburb = '$suburb' ";
			}else{
				$where.= "WHERE suburb = '$suburb' ";
			}
		}

		if( $data->bedrooms !=="" ){
			$bedrooms = $data->bedrooms;
			if($where != ""){
				$where.= "AND bedrooms = '$bedrooms' ";
			}else{
				$where.= "WHERE bedrooms = '$bedrooms' ";
			}
		}

		if( $data->ensuite ){
			$ensuite = $data->ensuite;
			if($where != ""){
				$where.= "AND ensuite > 0 ";
			}else{
				$where.= "WHERE ensuite > 0 ";
			}
		}

		if( $data->pool ){
			$pool = $data->pool;
			if($where != ""){
				$where.= "AND pool > 0 ";
			}else{
				$where.= "WHERE pool > 0 ";
			}
		}
		echo json_encode($this->properties->getFilteredProperties($where));

	}

}



