<?php

namespace Controllers;

use \Core\View,

\Helpers\Session,

\Helpers\Password,

\Helpers\Url;



class Auth extends \Core\Controller {

		

	private $_model;

	public function __construct(){

		$this->_model = new \Models\Auth();

	}

	

	public function login(){

		

		if(Session::get('loggedin')){

			Url::redirect('admin');

		}

		

		if(isset($_POST['submit'])){

			$username = $_POST['username'];

			$password = $_POST['password'];

			$dataword = $this->_model->getHash($username);

			if(!sha1($password) == $dataword){

				$error[] = 'Wrong Username and Password';

			}

			

			if(!$error){

				Session::set('loggedin',true);

				Session::set('userID',$this->_model->getID($username));

				

				$data = array('lastLogin' => date('Y-m-d G:i:s'));

				$where = array('userID' => $this->_model->getID($username));

				

				

				Url::redirect('admin');

				

			}

		}

		

		$data['title'] = 'Login';

		$data['name'] = 'Matt';

		View::rendertemplate('header', $data);

		View::render('auth/login', $data, $error);

		View::rendertemplate('footer', $data);

	}

	

	public function logout(){

		Session::destroy();

		Url::redirect();

	}

	

	

}