<?php
/**
 * Routes - all standard routes are defined here.
 *
 * @author David Carr - dave@daveismyname.com
 * @version 2.2
 * @date updated Sept 19, 2015
 */

/** Create alias for Router. */
use Core\Router;
use Helpers\Hooks;

/** Define routes. */
Router::any('', 'Controllers\Home@index');
Router::any('admin', 'Controllers\Admin@admin');
Router::any('login', 'Controllers\Auth@login');
Router::any('logout', 'Controllers\Auth@logout');
Router::any('home', 'Controllers\Home@index');

Router::any('getDistinctBedrooms', function(){
	$properties = new \Models\Property();
	echo json_encode($properties->getDistinctBedrooms());
});

Router::any('getDistinctSuburbs', function(){
	$properties = new \Models\Property();
	echo json_encode($properties->getDistinctSuburbs());
});

Router::any('getInitialProperties', function(){
	$properties = new \Models\Property();
	echo json_encode($properties->getInitialProperties());
});

Router::any('getFeatureProperties', function(){
	$properties = new \Models\Property();
	echo json_encode($properties->getFeatureProperties());
});

Router::any('getAllProperties', function(){
	$properties = new \Models\Property();
	echo json_encode($properties->getAllProperties());
});

Router::get('getProperty/(:any)', function($id){
	$properties = new \Models\Property();
	echo json_encode($properties->getProperty($id));
});

Router::post('getFilteredProperties', 'Controllers\Home@getFilteredProperties');
Router::post('update_property', 'Controllers\Admin@update');
Router::post('add_property', 'Controllers\Admin@add');
Router::post('delete_property', 'Controllers\Admin@delete');

/** Module routes. */
$hooks = Hooks::get();
$hooks->run('routes');

/** If no route found. */
Router::error('Core\Error@index');

/** Turn on old style routing. */
Router::$fallback = false;

/** Execute matched routes. */
Router::dispatch();
