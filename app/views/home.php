<?php

   use Helpers\URL;
   use helpers\session;


    ?>

    <section class="fixed">

        <nav class="top-bar" data-topbar role="navigation">

            <ul class="title-area">

                <li class="name">

                    <h1><a href="/"><?php echo $data['title'] ?> Realty</a></h1>

                </li>

                <li class="toggle-topbar menu-icon">

                    <a href="#">

                        <span>Menu</span>

                    </a>

                </li>

            </ul>

            <section class="top-bar-section">

                <!-- Right Nav Section -->

                <ul class="right">

                    <?php

					if(!Session::get('loggedin')){

						?><li class="active"><a href="<?php echo DIR;?>login">Login</a></li>

					<?php }else{ ?>

						<li class="warning"><a href="admin">Admin</a></li>

						<li class="active"><a href="<?php echo DIR;?>logout">Logout</a></li>

					<?php

					}

				?>

                </ul>

            </section>

        </nav>

    </section>

    <section class="row">

        <header class="small-12 column">

            <h3>Welcome to <?php echo $data['title'] ?> Realty</h3>

            <p>Your Palmerston North Real Estate Professional.</p>

        </header>

    </section>

    <section class="row" id="property-section">

        <!--Main Row -->

        <nav class="small-12 large-2 columns" id="">

            <h6 class="filter-heading"> Filters:</h6>

            <form id="filters" v-on="submit: filterListings">

                <!--search input box -->

                <div class="row">

                    <div class="large-12 columns" id="by_street">
                        <span class="success label small-label">Street Address</span>

                        <input v-model="property.address" v-on="keyup: filterListings" type="text" placeholder="Search by Street">

                    </div>

                </div>

                

                <!--select box -->

                <div class="row">

                    <div class="large-12 columns" id="suburb-select-list">

                        <span class="success label small-label">Area</span>

                        <select id="suburb-list" name="suburb" v-model="property.suburb">

                            <option>Select an Area</option>

                            <option v-repeat="suburbs">{{suburb}}</option>
                        </select>

                    </div>

                </div>



                <!--radio buttons -->

                <div class="row bedroom-row" id="bedroom-list">

                    <span class="success label small-label">Bedrooms</span>					

					<ul class="small-block-grid-6 medium-block-grid-3 large-block-grid-3">

						<li v-repeat="bedrooms" id="bed"><input type="radio" name="bedrooms" value="{{bedrooms}}" id="bed{{bedrooms}}" v-model="property.bedrooms"><label>{{bedrooms}}</label></li>

					</ul>

                </div>



                <!--checkboxes -->

                <div class="row additional-row">

                    <span class="success label small-label">Additional Features</span>

                    <div class="small-6 large-12 columns additional-rad-column">

                        <input id="ensuite" type="checkbox" name="ensuite" v-model="property.ensuite">

                        <label for="ensuite">Ensuite</label>

                    </div>



                    <div class="small-6 large-12 columns additional-rad-column">

                        <input id="pool" type="checkbox" name="pool" v-model="property.pool">

                        <label for="pool">Pool</label>

                    </div>

                </div>

                <input class="button small-12 large-12 columns radius" type="submit" value="Submit">

            </form>

        </nav> <!--end of form section-->



        <section>

            <div class="small-12 large-8 columns">

                <h6 class="filter-heading"> Listings:</h6>

                <div id="listed-properties">

                    <!-- Listed houses will go here-->

					<ul class="small-block-grid-1 large-block-grid-5">

						<li v-repeat="p: properties">

							<div class="listing-wrapper">

								<a href="#" class="th radius">

									<img src="<?php echo URL::templatePath();?>images/houses/houses_sm/{{p.image_name}}" class="list-image">

								</a>

								<p class="location-text"><span class="warning label lab"><b>${{p.list_price}}</b></span></p>

								<p class="location-text suburb"><span class="label small-label">{{p.address}}</span></p>

								<p class="location-text suburb">{{p.suburb}}</p>

								<p class="desc-text">Bedrooms:{{p.bedrooms}}</p>

								<p class="desc-text">Ensuite:{{p.ensuite}}</p>

								<p class="desc-text">Pool:{{p.pool}}</p>

								<p class="desc-text"><a href="#">More Details...</a></p>

							</div>

						<li>				

					</ul>

				</div>

            </div>

        </section>



        <section>

            <div class="small-12 large-2 columns">

                <h6 class="filter-heading"> Featured:</h6>

                <div id="featured-properties">

                    <!-- Featured houses will go here-->

					<ul class="small-block-grid-1 large-block-grid-1">

						<li v-repeat="p: featured">

							<div class="listing-wrapper">

								<a href="#" class="th radius">

									<img src="<?php echo URL::templatePath();?>images/houses/houses_sm/{{p.image_name}}" class="list-image">

								</a>

								<p class="location-text"><span class="alert label lab"><b>${{p.list_price}}</b></span></p>

								<p class="location-text suburb"><span class="label small-label">{{p.address}}</span></p>

								<p class="location-text suburb">{{p.suburb}}</p>

								<p class="desc-text">Bedrooms:{{p.bedrooms}}</p>

								<p class="desc-text">Ensuite:{{p.ensuite}}</p>

								<p class="desc-text">Pool:{{p.pool}}</p>

								<p class="desc-text"><a href="#">More Details...</a></p>

							</div>

						<li>				

					</ul>

                </div>

            </div>

        </section>

        <!--END Main Row -->

    

    </section>

