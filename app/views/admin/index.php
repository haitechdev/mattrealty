<?php

	use Helpers\URL;
	use Helpers\Session;

	

?>

    <section class="fixed">

        <nav class="top-bar" data-topbar role="navigation" id="admin-nav">

            <ul class="title-area">

                <li class="name">

                    <h1><a href="/"><?php echo $data['name'] ?> Realty</a></h1>

                </li>

                <li class="toggle-topbar menu-icon">

                    <a href="#">
                        <span>Menu</span>
                    </a>

                </li>

            </ul>

            <section class="top-bar-section">

                <!-- Right Nav Section -->

                <ul class="right">
			<li id="user"><span>logged in as <?php echo Session::get('userID'); ?></span></li>
                	<li class="active"><a href="logout">Logout</a></li>

                </ul>

            </section>

        </nav>

    </section>

	<div class="row full-width">
		<div class="large-8 small-12 columns">
			<h3> Admin Area </h3>
			<section id="table-section">
				<table>
					<thead>
						<tr>
							<th>#</th>
							<th>Address</th>
							<th>Suburb</th>
							<th>Town/City</th>
							<th>Bedrooms</th>
							<th>Ensuite</th>
							<th>Pool</th>
							<th>List Price</th>
							<th class="text-center">Active</th>
							<th class colspan="3">Actions -<a href="#" v-on="click :showAddForm"> [ Add New ] </a></th>
						</tr>
					</thead>
					<tbody>
						<tr v-repeat="property:properties">
							<td>{{property.id}}</td>
							<td v-on="click" :!disabled>{{property.address}}</td>
							<td>{{property.suburb}}</td>
							<td>{{property.town_city}}</td>
							<td>{{property.bedrooms}}</td>
							<td>{{property.ensuite}}</td>
							<td>{{property.pool}}</td>
							<td>{{property.list_price}}</td>
							<td>{{property.active}}</td>
							<td><a href="#" v-on="click :showDetails(property.id)"><i class="fa fa-file-text-o"></i></a></td>
							<td><a href="#" v-on="click :showEditForm(property.id)"><i class="fa fa-pencil"></i></a></td>
							<td><a href="#" v-on="click :deleteProperty(property.id)"><i class="fa fa-close"></i></a></td>
						</tr>
					</tbody>
				</table>
			</section>
		</div>

		<div class="large-2 large-offset-2 small-12 columns">
			<section id="property-edit">
				<form action="" id="property-form" v-show="editFormIsVisible" v-on="submit :updateProperty">
						<input type="hidden" v-model="editedProperty[0].id">
					<label>Street Address:
						<input type="text" v-model="editedProperty[0].address" name="address" required>
					</label>
					<label>Suburb:
						<input type="text" v-model="editedProperty[0].suburb" name="suburb" required>
					</label>
					<label>Town/City:
						<input type="text" v-model="editedProperty[0].town_city" name="town_city" required>
					</label>
					<label>List Price:
						<input type="text" v-model="editedProperty[0].list_price" name="list_price" required>
					</label>
					<label>Bedrooms:
						<input type="text" v-model="editedProperty[0].bedrooms" name="bedrooms" required>
					</label>
						<input type="checkbox" v-model="editedProperty[0].ensuite" name="ensuite"><label>Ensuite:</label>
						<input type="checkbox" v-model="editedProperty[0].pool" name="pool"><label>Pool:</label>
						<input type="checkbox" v-model="editedProperty[0].active" name="active"><label>Active:</label>
					<input class="button small-12 large-12 columns radius" type="submit" value="Update">
				</form>
			</section>
		</div>

		<div class="large-2 large-offset-2 small-12 columns">
			<section id="property-add">
				<form action="" id="property-form" v-show="addFormIsVisible" v-on="submit :add">
					<label>Street Address:
						<input type="text" v-model="addProperty[0].address" name="address" required>
					</label>
					<label>Suburb:
						<input type="text" v-model="addProperty[0].suburb" name="suburb" required>
					</label>
					<label>Town/City:
						<input type="text" v-model="addProperty[0].town_city" name="town_city" required>
					</label>
					<label>List Price:
						<input type="text" v-model="addProperty[0].list_price" name="list_price" required>
					</label>
					<label>Bedrooms:
						<input type="text" v-model="addProperty[0].bedrooms" name="bedrooms" required>
					</label>
						<input type="checkbox" v-model="addProperty[0].ensuite" name="ensuite"><label>Ensuite:</label>
						<input type="checkbox" v-model="addProperty[0].pool" name="pool"><label>Pool:</label>
						<input type="checkbox" v-model="addProperty[0].active" name="active"><label>Active:</label>

					<input class="button small-12 large-12 columns radius" type="submit" value="Add">
				</form>
			</section>
		</div>

		<div class="large-2 large-offset-2 small-12 columns">
			<section id="property-view" >
				<div v-show="viewFormIsVisible">
					<span class="view-listing">Address: </span><span>{{ viewProperty[0].address}}</span><br>
					<span class="view-listing">Suburb: </span><span>{{ viewProperty[0].suburb }}</span><br>
					<span class="view-listing">Town/City: </span><span>{{ viewProperty[0].town_city}}</span><br>
					<span class="view-listing">No. of Bedrooms: </span><span>{{ viewProperty[0].bedrooms }}</span><br>
					<span class="view-listing">Has ensuite?: </span><span>{{ viewProperty[0].ensuite }}</span><br>
					<span class="view-listing">Has pool?: </span><span>{{ viewProperty[0].pool }}</span><br>
					<span class="view-listing">Is active?: </span><span>{{ viewProperty[0].active }}</span><br>
					<span class="view-listing">Post Code: </span><span>{{ viewProperty[0].post_code}}</span><br>
					<span class="view-listing">Lon: </span><span>{{ viewProperty[0].lon }}</span><br>
					<span class="view-listing">Lat: </span><span>{{ viewProperty[0].lat }}</span><br>
					<span class="view-listing">Featured?: </span><span>{{ viewProperty[0].featured }}</span><br>
					<span class="view-listing">List Price: </span><span>{{ viewProperty[0].list_price }}</span><br>
					<span class="view-listing">Image name: </span><span>{{ viewProperty[0].image_name }}</span><br>
				</div>
			</section>
		</div>
	</div>