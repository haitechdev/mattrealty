<?php

	use Helpers\URL;

	use Helpers\Form;

	use Core\Error;

?>

    <section class="fixed">

        <nav class="top-bar" data-topbar role="navigation">

            <ul class="title-area">

                <li class="name">

                    <h1><a href="/"><?php echo $data['name'] ?> Realty</a></h1>

                </li>

                <li class="toggle-topbar menu-icon">

                    <a href="#">

                        <span>Menu</span>

                    </a>

                </li>

            </ul>

        </nav>

    </section>

	<div id="login_form">

		<?php echo Form::open(array('method' => 'post'));?>

		<p class="login-status"><?php echo Error::display($error); ?></p>

		<?php echo Form::input(array('name' => 'username', 'placeholder' => 'Username' ));?>

		<?php echo Form::input(array('name' => 'password', 'type' => 'password', 'placeholder' => 'Password' ));?>

		<?php echo Form::button(array('name' => 'submit', 'type' => 'submit', 'value' => 'Login' ));?>

	</div>

	<style>

		button {

			width: 100% !important;

		}

		

		.alert.alert-danger {

			color: red;

		}

	</style>