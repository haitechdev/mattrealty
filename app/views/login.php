<?php
	use Helpers\URL;
	use Helpers\form;
	use Core\error;
	
	
    ?>
    <section class="fixed">
        <nav class="top-bar" data-topbar role="navigation">
            <ul class="title-area">
                <li class="name">
                    <h1><a href="#"><?php echo $data['name'] ?> Realty</a></h1>
                </li>
                <li class="toggle-topbar menu-icon">
                    <a href="#">
                        <span>Menu</span>
                    </a>
                </li>
            </ul>
        </nav>
    </section>
	
	<?php echo Error::display($error); ?>
	
	<?php echo Form::open(array('method' => 'post'));?>
	<?php echo Form::input(array('name' => 'username', 'placeholder' => 'Username' ));?>
	<?php echo Form::input(array('name' => 'password', 'type' => 'password', 'placeholder' => 'Password' ));?>
	<?php echo Form::input(array('name' => 'submit', 'type' => 'submit', 'value' => 'Login' ));?>
	<!--<div id="example">	
		<form id="login_form" method="post" action="admin">
			<p id="login_result"></p>
			<input id="username" v-model="username" name="username" type="text" placeholder="Username" /><br>
			<input id="password" v-model="password" name="password" type="password" placeholder="Password" /><br>
			<button id="login_button" name="login" v-on:click="login">Login</button>
			
		</form>
		
		<style>
			button#login_button {
				width: 100% !important;
			}
		</style>-->
	</div>