<?php
use Helpers\Url;
?>
	<!DOCTYPE html>
	<html lang="<?php echo LANGUAGE_CODE; ?>">

	<head>
		<!-- Site meta -->
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
			<title>
				<?php echo $data['title'].' - '.SITETITLE; //SITETITLE defined in app/Core/Config.php ?>
			</title>

			<!-- CSS -->
			<link rel="stylesheet" href="<?php echo URL::templatePath();?>css/foundation.min.css">
			<link rel="stylesheet" href="<?php echo URL::templatePath();?>css/styles.css">
			<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">


			<!-- JS -->
			<script type="text/javascript" src="<?php echo URL::templatePath();?>js/modernizr.js"></script>

	</head>
	<body>