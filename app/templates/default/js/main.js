var filterModel = new Vue({

	el: '#property-section',

	

	ready: function() {

		this.fetchInitialProperties();
		this.fetchFeatureProperties();
		this.fetchDistinctSuburbs();
		this.fetchDistinctBedrooms();

	},

	

	data: {
		properties: [],
		featured: [],
		property: {
			id: '', 
			address: '', 
			suburb: '', 
			postcode: '', 
			lon: '', 
			lat: '', 
			town_city: '', 
			bedrooms: '', 
			ensuite: '', 
			pool: '', 
			featured: '', 
			list_price: '', 
			image_name: '', 
			active: '',
		},

	},

	

	methods: {

		fetchDistinctSuburbs() {
                	this.$http.get('getDistinctSuburbs', function (suburbs) {
                   		this.$set('suburbs', suburbs);
                	});
            	},


        	fetchDistinctBedrooms() {
            		this.$http.get('getDistinctBedrooms', function (bedrooms) {
                		this.$set('bedrooms', bedrooms);
		        });
		},

		fetchFeatureProperties() {
			this.$http.get('getFeatureProperties', function(featured) {
				this.featured = featured;
			});
		},

		fetchInitialProperties() {
			this.$http.get('getInitialProperties', function(properties) {
				this.properties = properties;
			});
		},

		filterListings: function (e) {
			e.preventDefault();
			this.$http.post('getFilteredProperties', this.property, function (response) {
				this.$set('properties', response);
			});
		},
		

	}

});



var adminModel = new Vue({

	el: '#table-section',

	

	ready: function() {

		this.getAllProperties();

	},

	

	data: {
		properties: [],
		property: {
			id: '', 
			address: '', 
			suburb: '', 
			town_city: '', 
			bedrooms: '', 
			ensuite: '', 
			pool: '', 
			list_price: '', 
			active: '',
		},

	},

	

	methods: {


		getAllProperties: function() {
			this.$http.get('getAllProperties', function(properties) {
				this.properties = properties;
			});
		},
		
		deleteProperty: function (id) {
			var result = confirm("Are you sure you want to delete property record " + id);
			if (result == true) {
				this.$http.post('delete_property', id, function (response) {
					adminModel.getAllProperties();
				});
			} else {
    				
			}

		},

		hideDetails: function() {
			editModel.editFormIsVisible = false;
			addModel.addFormIsVisible = false;
			viewModel.viewFormIsVisible = false;
		},

		showEditForm: function (id) {
			this.hideDetails();
			editModel.showPropertyEditForm(id);
		},

		showAddForm: function () {
			this.hideDetails();
			addModel.showPropertyAddForm();
		},

		showDetails: function (id) {
			this.hideDetails();
			viewModel.showPropertyDetails(id);
		},

	}

});



var editModel = new Vue({
	el: '#property-edit',

	data: {
		editFormIsVisible: false,
		editedProperty: {
			id: '',
			address: '',
			suburb: '',
			town_city: '',
			list_price: '',
			bedrooms: '',
			ensuite: '',
			pool: '',
			list_price: '',
			active: '',
		}
	},

	methods: {
		showPropertyEditForm: function (id) {
			var resource = this.$resource('getProperty/:id');
			resource.get({
				id: id
			}, function (response) {
				this.$set('editedProperty', response);
				this.editFormIsVisible = true;
			});	
		},

		updateProperty: function (e) {
			e.preventDefault();
			var editedProperty = this.editedProperty[0];
			this.$http.post('update_property', editedProperty, function (response) {
				adminModel.getAllProperties();
			});
		}
	}
});


var addModel = new Vue({
	el: '#property-add',

	data: {
		addFormIsVisible: false,
		addProperty: {
			id: '',
			address: '',
			suburb: '',
			town_city: '',
			list_price: '',
			bedrooms: '',
			ensuite: '',
			pool: '',
			list_price: '',
			active: '',
		},
	},

	methods: {
		showPropertyAddForm: function () {
				this.addFormIsVisible = true;
		},

		add: function(e){
			e.preventDefault();
			var addProperty = this.addProperty[0];
			this.$http.post('add_property', addProperty, function (response) {
				adminModel.getAllProperties();
			});

		},

	}
});

var viewModel = new Vue({
	el: '#property-view',

	data: {
		viewFormIsVisible: false,
		viewProperty: {

		}
	},

	methods: {
		showPropertyDetails: function (id) {
			var resource = this.$resource('getProperty/:id');
			resource.get({
				id: id
			}, function (response) {
				this.$set('viewProperty', response);
				this.viewFormIsVisible = true;
			});	
		},

	}
});