<?php
  use Helpers\Url;
?>
<!-- JS -->   
<script type="text/javascript" src="<?php echo URL::templatePath();?>js/jquery.js"></script> 
<script type="text/javascript" src="<?php echo URL::templatePath();?>js/foundation.min.js"></script>
<script type="text/javascript" src="<?php echo URL::templatePath();?>js/vue.min.js"></script>
<script type="text/javascript" src="<?php echo URL::templatePath();?>js/vue-resource.js"></script>
<script type="text/javascript" src="<?php echo URL::templatePath();?>js/main.js"></script>
<script>
  $(document).foundation();
</script>
</body>
</html>